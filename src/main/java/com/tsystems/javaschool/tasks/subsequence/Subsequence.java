package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

import static java.util.Objects.isNull;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        //сначала проверка тривиальных случаев, потом проверка обратным ходом
        if(isNull(x) || isNull(y)){
            throw new IllegalArgumentException();
        } else if(x.equals(y)){
            return true;
        } else if(!y.containsAll(x)){
            return false;
        } else if(y.retainAll(x)){
            if(y.size()!=x.size()){
                boolean result = false;
                int count = 0;
                ListIterator xListIterator = x.listIterator(x.size());
                ListIterator yListIterator = y.listIterator(y.size());
                while(xListIterator.hasPrevious()){
                    Object xValue = xListIterator.previous();
                    while(yListIterator.hasPrevious()){
                        Object yValue = yListIterator.previous();
                        if (xValue.equals(yValue)){
                            count++;
                            break;
                        }
                    }
                }
                if (count == x.size()){
                    result = true;
                }
                return result;
            }
            return y.equals(x);
        }
        return false;
    }
}

package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers){
        // TODO : Implement your solution here
        //проверка, что null не входит
        if(inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }

        //сортировка и проверка, что нет повторяющихся значений
        TreeSet<Integer> setOfInputs = new TreeSet<>(inputNumbers);
        if(setOfInputs.size()!=inputNumbers.size()){
            throw new CannotBuildPyramidException();
        }

        //проверка, что чисел нужное количество
        int discr = 1+8*setOfInputs.size();
        int roundedSqrtOfDiscr = (int)Math.sqrt(discr);
        if(roundedSqrtOfDiscr*roundedSqrtOfDiscr!=discr){
            throw new CannotBuildPyramidException();
        }

        //количество цифр на последнем уровне и четность количества уровней
        int n = (roundedSqrtOfDiscr - 1)/2;
        int k = 2*n - 1;
        int parity = (n-1)%2;

        //инициализация конечного массива
        int[][] array = new int[n][k];
        int countOfContinuesEnd;
        int countOfContinuesStart;

        Iterator<Integer> descIterator = setOfInputs.descendingIterator();
        for (int i = n-1; i >= 0; i--) {
            countOfContinuesEnd = (n-1-i)/2;
            countOfContinuesStart = (n-1-i)/2;
            int columnFlag1 = n-1-i;
            for (int j = k-1; j >= 0; j--) {
                if((i+j)%2==parity && countOfContinuesStart!=0 && (columnFlag1-2)==j){
                    columnFlag1-=2;
                    countOfContinuesEnd--;
                    continue;
                }
                if((i+j)%2==parity && countOfContinuesEnd!=0){
                    countOfContinuesEnd--;
                    continue;
                }
                if((i+j)%2==parity && descIterator.hasNext() && countOfContinuesEnd==0){
                    array[i][j] = descIterator.next();
                }
            }
        }
        return array;
    }


}

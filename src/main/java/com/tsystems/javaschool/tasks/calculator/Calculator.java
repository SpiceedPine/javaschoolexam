package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        /* Понимаю, что решение слишком громоздкое и сложное, задания в университете не предполагали знание
           регулярных выражений, поэтому учил и приспосабливался по ходу выполнения задания.
        */

        if (statement==null){
            return null;
        } else if(statement.isEmpty()){
            return null;
        }


        boolean plusChecker = Pattern.matches(".*\\+\\+.*",statement);
        boolean minusChecker = Pattern.matches(".*--.*",statement);
        boolean multiplyChecker = Pattern.matches(".*\\*\\*.*",statement);
        boolean divideChecker = Pattern.matches(".*//.*",statement);
        boolean dotChecker = Pattern.matches(".*\\.\\..*",statement);
        boolean invalidChecker = Pattern.matches(".*[^0-9.*/()+-].*",statement);

        if(plusChecker || minusChecker || multiplyChecker || divideChecker || dotChecker || invalidChecker){
            return null;
        }

        if(Pattern.matches("[^(]*?\\).*",statement)){
            return null;
        }

        Pattern leftBra = Pattern.compile(".*?\\(.+?");
        Pattern rightBra = Pattern.compile(".+?\\).*?");
        Matcher leftMatcher = leftBra.matcher(statement);
        Matcher rightMatcher = rightBra.matcher(statement);

        //Лишние правые скобки??
        while(leftMatcher.find()){
            if(!rightMatcher.find()){
                return null;
            }
        }

        statement = "("+statement+")";
        String number = "-?\\d+(\\.\\d+)?";
        String positiveNumber = "\\d+(\\.\\d+)?"; //аккуратнее
        String addStringPattern = "(?:.*?)"+number+"\\+"+number+"(?:.*?)";
        String deductStringPattern = "(?:.*?)"+number+"-"+number+"(?:.*?)";
        String multiplyStringPattern = "(?:.*?)"+number+"\\*"+number+"(?:.*?)";
        String divideStringPattern = "(?:.*?)"+number+"/"+number+"(?:.*?)";
        String signsStringPattern = "[-+*/]";
        String leftBoxingStringPattern = "\\)[-+*/]"+number;
        String rightBoxingStringPattern = number+"[-+*/]\\(";
        String aloneLeftStringPattern = "\\)[^(]+?[0-9]+?.*?[()]";
        String aloneRightStringPattern = "\\([^()]*?[0-9]+?[^)]*?[(]";
        String aloneStringPattern = aloneLeftStringPattern+"|"+aloneRightStringPattern;

        Pattern numberPattern = Pattern.compile(number);
        Pattern positiveNumberPattern = Pattern.compile(positiveNumber); //аккуратнее
        Pattern parenthesesPattern = Pattern.compile("(\\()[^(]+?(\\))");
        Pattern simpleAddPattern = Pattern.compile(number+"\\+"+number);
        Pattern simpleDeductPattern = Pattern.compile(number+"-"+number);
        Pattern simpleMultiplyPattern = Pattern.compile(number+"\\*"+number);
        Pattern simpleDividePattern = Pattern.compile(number+"/"+number);
        Pattern leftNumbersBoxingPattern = Pattern.compile("\\)[-+*/]"+number);
        Pattern rightNumbersBoxingPattern = Pattern.compile(number+"[-+*/]\\(");
        Pattern aloneNumberPattern = Pattern.compile(aloneStringPattern);
        Pattern innerParethesesPattern = Pattern.compile("([-+*/]?)(?:\\()[^(]+?(?:\\))|(?:\\()[^(]+?(?:\\))([-+*/]?)");

        while(Pattern.matches(".*"+leftBoxingStringPattern+".*",statement)) {
            Matcher leftNumbersBoxingMatcher = leftNumbersBoxingPattern.matcher(statement);
            if (leftNumbersBoxingMatcher.find()) {
                String target = statement.substring(leftNumbersBoxingMatcher.start(), leftNumbersBoxingMatcher.end());
                String replacement = target.substring(0, 2) + "(" + target.substring(2) + ")";
                statement = statement.replace(target, replacement);
            }
        }

        while(Pattern.matches(".*"+rightBoxingStringPattern+".*",statement)) {
            Matcher rightNumbersBoxingMatcher = rightNumbersBoxingPattern.matcher(statement);
            if (rightNumbersBoxingMatcher.find()) {
                String target = statement.substring(rightNumbersBoxingMatcher.start(), rightNumbersBoxingMatcher.end());
                int length = target.length();
                String replacement = "(" + target.substring(0, length - 2) + ")" + target.substring(length - 2);
                statement = statement.replace(target, replacement);
            }
        }

        while(Pattern.matches(".*"+aloneStringPattern+".*",statement)){
            Matcher aloneNumberMatcher = aloneNumberPattern.matcher(statement);
            if (aloneNumberMatcher.find()) {
                String target = statement.substring(aloneNumberMatcher.start(), aloneNumberMatcher.end());
                String replacement = target;
                Matcher numberMatcher = positiveNumberPattern.matcher(replacement);
                if (numberMatcher.find()) {
                    String subTarget = replacement.substring(numberMatcher.start(), numberMatcher.end());
                    String subReplacement = "(" + subTarget + ")";
                    replacement = target.replace(subTarget, subReplacement);
                }
                statement = statement.replace(target, replacement);
            }
        }

        Matcher parenthesesMatcher = parenthesesPattern.matcher(statement);
        ArrayList<String> simpleStatements = new ArrayList<>();
        while(parenthesesMatcher.find()){
            simpleStatements.add(statement.substring(parenthesesMatcher.start()+1, parenthesesMatcher.end()-1));
        }

        while(simpleStatements.size()!=1 || (Pattern.matches(multiplyStringPattern,simpleStatements.get(0))
                || Pattern.matches(divideStringPattern,simpleStatements.get(0))
                || Pattern.matches(deductStringPattern,simpleStatements.get(0))
                || Pattern.matches(addStringPattern,simpleStatements.get(0)))) {
            Matcher innerParethesesMatcher = innerParethesesPattern.matcher(statement);
            ArrayList<String> innerSigns = new ArrayList<>();

            while (innerParethesesMatcher.find()) {
                String inner = statement.substring(innerParethesesMatcher.start(), innerParethesesMatcher.end());
                if (Pattern.matches(signsStringPattern, inner.substring(0, 1))
                        || innerParethesesMatcher.start() == 0
                        || innerParethesesMatcher.start() == 1) {
                    innerSigns.add(inner.substring(0, 1));
                } else if (innerParethesesMatcher.start() != 0) {
                    innerSigns.add(statement.substring(innerParethesesMatcher.start() - 2, innerParethesesMatcher.start()));
                }
            }

            for (String simpleStatement : simpleStatements) {
                int index = simpleStatements.indexOf(simpleStatement);
                while (Pattern.matches(addStringPattern, simpleStatement)
                        || Pattern.matches(deductStringPattern, simpleStatement)
                        || Pattern.matches(multiplyStringPattern, simpleStatement)
                        || Pattern.matches(divideStringPattern, simpleStatement)) {

                    while(Pattern.matches(".*"+divideStringPattern+".*",simpleStatement)) {
                        Matcher divideMatcher = simpleDividePattern.matcher(simpleStatement);
                        if (divideMatcher.find()) {
                            String divExpression = simpleStatement.substring(divideMatcher.start(), divideMatcher.end());
                            double divResult = 1;
                            Matcher numberMatcher = numberPattern.matcher(divExpression);
                            if (numberMatcher.find()) {
                                divResult *= Double.parseDouble(divExpression.substring(numberMatcher.start(), numberMatcher.end()));
                            }
                            while (numberMatcher.find()) {
                                double denom = Double.parseDouble(divExpression.substring(numberMatcher.start(), numberMatcher.end()));
                                if (denom != 0) {
                                    divResult /= denom;
                                } else {
                                    return null;
                                }
                            }
                            simpleStatement = simpleStatement.replace(divExpression, "" + divResult);
                        }
                    }

                    while(Pattern.matches(".*"+multiplyStringPattern+".*",simpleStatement)) {
                        Matcher multiplyMatcher = simpleMultiplyPattern.matcher(simpleStatement);
                        if (multiplyMatcher.find()) {
                            String multExpression = simpleStatement.substring(multiplyMatcher.start(), multiplyMatcher.end());
                            double multResult = 1;
                            Matcher numberMatcher = numberPattern.matcher(multExpression);
                            while (numberMatcher.find()) {
                                multResult *= Double.parseDouble(multExpression.substring(numberMatcher.start(), numberMatcher.end()));
                            }
                            simpleStatement = simpleStatement.replace(multExpression, "" + multResult);
                        }
                    }

                    while(Pattern.matches(".*"+deductStringPattern+".*",simpleStatement)) {
                        Matcher dedMatcher = simpleDeductPattern.matcher(simpleStatement);
                        if (dedMatcher.find()) {
                            String dedExpression = simpleStatement.substring(dedMatcher.start(), dedMatcher.end());
                            double dedResult = 0;
                            Matcher numberMatcher = numberPattern.matcher(dedExpression);
                            while (numberMatcher.find()) {
                                dedResult += Double.parseDouble(dedExpression.substring(numberMatcher.start(), numberMatcher.end()));
                            }
                            simpleStatement = simpleStatement.replace(dedExpression, "" + dedResult);
                        }
                    }

                    while(Pattern.matches(".*"+addStringPattern+".*",simpleStatement)) {
                        Matcher addMatcher = simpleAddPattern.matcher(simpleStatement);
                        if (addMatcher.find()) {
                            String addExpression = simpleStatement.substring(addMatcher.start(), addMatcher.end());
                            double addResult = 0;
                            Matcher numberMatcher = numberPattern.matcher(addExpression);
                            while (numberMatcher.find()) {
                                addResult += Double.parseDouble(addExpression.substring(numberMatcher.start(), numberMatcher.end()));
                            }
                            simpleStatement = simpleStatement.replace(addExpression, "" + addResult);
                        }
                    }
                }
                simpleStatements.set(index, simpleStatement);
            }

            if (innerSigns.size() > 1) {
                ArrayList<String> newSimpleStatements = new ArrayList<>();
                boolean flag = false;
                int flagCount = 1;
                for (int i = 0; i < innerSigns.size(); i++) {
                    if (Pattern.matches(signsStringPattern, innerSigns.get(i))) {
                        String newSimple = "";
                        if(flag) {
                            newSimple += newSimpleStatements.get(i-flagCount) + innerSigns.get(i) + simpleStatements.get(i);
                            newSimpleStatements.set(i-flagCount, newSimple);
                        }else{
                            newSimple += simpleStatements.get(i - 1) + innerSigns.get(i) + simpleStatements.get(i);
                            newSimpleStatements.add(newSimple);
                        }
                        flag = true;
                        flagCount++;
                    }else{
                        flag = false;
                    }
                }
                simpleStatements = newSimpleStatements;

                StringBuilder newStatement = new StringBuilder();
                Iterator<String> iter = simpleStatements.iterator();
                while (iter.hasNext()) {
                    for (String innerSign : innerSigns) {
                        if (innerSign.equals("(")) {
                            newStatement.append(innerSign).append(iter.next());
                        } else if (innerSign.length() == 2) {
                            newStatement.append(")").append(innerSign).append(iter.next()).append(")");
                            newStatement.insert(0, "(");
                        }
                    }
                }

                statement = newStatement + ")";
                statement = statement.replaceAll("--", "+");
                for (int i = 0; i < simpleStatements.size(); i++) {
                    String replacement = simpleStatements.get(i);
                    if (replacement.contains("--")) {
                        replacement = replacement.replaceAll("--", "+");
                        simpleStatements.set(i, replacement);
                    }
                }
            }
        }

        if (simpleStatements.size() == 1) {
            String result;
            double value = Double.parseDouble(simpleStatements.get(0));
            if (value % 1 == 0) {
                result = "" + (int) value;
                return result;
            } else if (value * 10000 % 1 != 0) {
                result = String.format("%.4f", value);
                result = result.replace(',', '.');
                return result;
            } else {
                result = "" + value;
                return result;
            }
        }
        return "";
    }

}
